FROM gcc:9.2.0

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
	bison \
	flex \
 && rm -rf /var/lib/apt/lists/*