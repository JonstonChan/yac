.PHONY: all clean

CFLAGS=-g -Wall -Werror
LEX=flex
YACC=bison -y
YFLAGS=-d

all: calculator

calculator: parser.o lexer.o math.o main.o
	$(CC) -o $@ $(LDFLAGS) $^

parser.o: parser.y

lexer.o: lexer.l

math.o: math.c

main.o: main.c

clean:
	rm -rf *.o calculator