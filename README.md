Introduction
===

A calculator written in C supporting basic math operations and symbols:

- Addition (`+`)
- Subtraction and unary minus (`-`)
- Multiplication (`*`)
- Integer division (`/`, rounds to nearest integer towards zero)
- Exponentiation (`^`, non-negative powers only)
- Parentheses (`(` and `)`)
- Integers (`-2^31` to `2^31 - 1`)

Flex (lexer) is used for tokenization (splits `"1+20"` into `["1", "+", "20"]`)
and Bison (parser) is used for the grammar (`2+-4` becomes `2 + (-4)`)

If it cannot be parsed (for example, `2 $ 4`),
the exit code is set to `1` to indicate a failure


Sample runs
===

```console
# Interactive input
> ./calculator
1 + 1
2

# Scripted input
> ./calculator <<< '1 + 1'
2

# Order of precedence is respected
> ./calculator <<< '1 + 2 * 3'
7

# Spaces are not necessary
# and multi-digit integers are supported
> ./calculator <<< '-12+2*3'
-6

# All supported symbols
# 1 + 2 - (3 * 4) / 5 ^ 2 = 3 - 12 / 25 = 3 - 0 = 3
> ./calculator <<< '1 + 2 - (3 * 4) / 5 ^ 2'
3

# Invalid input gives an exit code
> ./calculator <<< '$'
> echo $?
1
```

You can try it out yourself on Linux-based operating systems by
building the executable yourself, or by using the [pre-built binary][calculator-executable]
(`chmod +x calculator` before running may be needed)


Test cases
===

- See `tests/parser/test` and `tests/lexer/test` for how to run automated tests
  - Inputs: `tests/input-files`
  - Expected outputs: `tests/lexer/output-expected` and `tests/parser/output-expected`


[calculator-executable]: https://gitlab.com/JonstonChan/yac/-/jobs/artifacts/master/raw/calculator/?job=executable
