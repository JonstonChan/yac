%{

#include <stdio.h>
#include <stdlib.h>

#include "y.tab.h"

%}


%option noyywrap
%option noinput
%option nounput

INTEGER		0|([1-9]+[0-9]*)


%%

"*"					{ return T_ASTERISK; }
"^"					{ return T_CARET; }
{INTEGER}			{ yylval.integer = atoi(yytext); return T_INTCONSTANT; }
"("					{ return T_LPAREN; }
"-"					{ return T_MINUS; }
"+"					{ return T_PLUS; }
")"					{ return T_RPAREN; }
"/"					{ return T_SLASH; }

[\n ]+				{ }
.					{ exit(EXIT_FAILURE); }
%%