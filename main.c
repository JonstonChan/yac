#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "y.tab.h"


typedef struct yy_buffer_state* YY_BUFFER_STATE;

extern YY_BUFFER_STATE yy_scan_string(const char* str);
extern void yy_delete_buffer(YY_BUFFER_STATE buffer);


int main() {
	calculate_user_input();

	exit(EXIT_SUCCESS);
}

int calculate(const char* equation) {
	int calculated_value;

	YY_BUFFER_STATE buffer = yy_scan_string(equation);
	int retval = yyparse(&calculated_value);
	yy_delete_buffer(buffer);

	if (retval != 0) {
		printf("Parsing failed\n");
		exit(EXIT_FAILURE);
	}

	return calculated_value;
}

void calculate_user_input() {
	char* equation = NULL;
	size_t equation_size;
	ssize_t num_chars_read = getline(&equation, &equation_size, stdin);

	if (num_chars_read == -1) {
		printf("Cannot read line into buffer\n");
		exit(EXIT_FAILURE);
	}

	int calculated_value = calculate(equation);

	printf("%d\n", calculated_value);

	free(equation);
}