#ifndef MATH_H
#define MATH_H

// Integer exponentiation function taken from https://stackoverflow.com/a/101613
int int_pow(int base, int exp);

#endif