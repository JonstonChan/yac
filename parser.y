%{
#include <stdio.h>

#include "math.h"

extern int yylex(void);

void yyerror(int* calculated_value, const char* error_message);

%}


%parse-param {int* calculated_value}

%union {
	int integer;
}

%token T_ASTERISK
%token T_CARET
%token T_LPAREN
%token T_MINUS
%token T_PLUS
%token T_RPAREN
%token T_SLASH

%token <integer> T_INTCONSTANT

%type <integer> BinaryExpression
%type <integer> Constant
%type <integer> Expression
%type <integer> UnaryExpression

%left T_MINUS T_PLUS
%left T_SLASH T_ASTERISK
%left T_CARET
%right UNARY_MINUS


%%

Start:
	  Expression
		{
			int value = $1;
			*calculated_value = value;
		}

Expression:
	  BinaryExpression
	| Constant
	| UnaryExpression
	| T_LPAREN Expression T_RPAREN
		{
			$$ = $2;
		}

BinaryExpression:
	  Expression T_ASTERISK Expression
		{
			int left = $1;
			int right = $3;

			int value = left * right;

			$$ = value;
		}
	| Expression T_CARET Expression
		{
			int left = $1;
			int right = $3;

			int value = int_pow(left, right);

			$$ = value;
		}
	| Expression T_MINUS Expression
		{
			int left = $1;
			int right = $3;

			int value = left - right;

			$$ = value;
		}
	| Expression T_PLUS Expression
		{
			int left = $1;
			int right = $3;

			int value = left + right;

			$$ = value;
		}
	| Expression T_SLASH Expression
		{
			int left = $1;
			int right = $3;

			int value = left / right;

			$$ = value;
		}

Constant:
	  T_INTCONSTANT

UnaryExpression:
	  T_MINUS Expression
		{
			int value = -$2;

			$$ = value;
		}

%%


void yyerror(int* calculated_value, const char* error_message) {
	fprintf(stderr, "%s\n", error_message);
}